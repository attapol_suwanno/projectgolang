package models

import (
	"apigallery/rand"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"hash"

	"github.com/jinzhu/gorm"
	"golang.org/x/crypto/bcrypt"
)

const key = 12
const hmacKey = "min"

type User struct {
	gorm.Model
	Name     string `gorm:"not null" json:"name`
	Email    string `gorm:"unique_index;not null" json:"email"`
	Password string `gorm:"not null" json:"password"`
	Token    string `gorm:"unique_index"`
	Picture  string `json:"picture"`
}

type UserService interface {
	UserSignup(user *User) error
	UserLoginDB(user *User) (string, error)
	GetUserByToken(token string) (*User, error)
	UserLogOut(user *User) error
}

type UserGorm struct {
	db   *gorm.DB
	hmac hash.Hash
}

type ULogin struct {
	Email    string
	Password string
}

func NewUserService(db *gorm.DB, key string) UserService {
	mac := hmac.New(sha256.New, []byte(key))
	return &UserGorm{db, mac}
}

func (gsu *UserGorm) UserSignup(user *User) error {
	const cost = 12
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), cost)
	if err != nil {
		return err
	}

	user.Password = string(hash)
	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	user.Token = token
	return gsu.db.Create(user).Error
}

func (gsu *UserGorm) UserLoginDB(user *User) (string, error) {
	found := new(User)
	err := gsu.db.Where("email=? ", user.Email).First(&found).Error
	if err != nil {
		return "", errors.New("not found email!")
	}
	err = bcrypt.CompareHashAndPassword([]byte(found.Password), []byte(user.Password))
	if err != nil {
		return "", errors.New("password is wrong!")
	}

	token, err := rand.GetToken()
	if err != nil {
		return "", err
	}

	gsu.hmac.Write([]byte(token))
	newToken := gsu.hmac.Sum(nil)
	gsu.hmac.Reset()

	err = gsu.db.Model(&User{}).Where("id=?", found.ID).Update("token", base64.URLEncoding.EncodeToString(newToken)).Error
	if err != nil {
		return "", err
	}
	return token, nil
}

func (gsu *UserGorm) GetUserByToken(token string) (*User, error) {
	user := new(User)
	gsu.hmac.Write([]byte(token))
	newToken := gsu.hmac.Sum(nil)
	gsu.hmac.Reset()
	err := gsu.db.Where("token = ?", base64.URLEncoding.EncodeToString(newToken)).First(user).Error
	if err != nil {
		return nil, err
	}
	fmt.Printf("Userid Login %v", user)
	return user, nil
}

func (gsu *UserGorm) UserLogOut(user *User) error {

	token, err := rand.GetToken()
	if err != nil {
		return err
	}
	gsu.hmac.Write([]byte(token))
	newToken := gsu.hmac.Sum(nil)
	gsu.hmac.Reset()

	err = gsu.db.Model(&user).Where("id=?", user.ID).Update("token", base64.URLEncoding.EncodeToString(newToken)).Error
	if err != nil {
		return err
	}
	return err
}
