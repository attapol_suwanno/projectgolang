package models

import (
	// "fmt"

	"fmt"
	"log"
	"os"
	"path/filepath"

	"github.com/jinzhu/gorm"
)

type Image struct {
	gorm.Model
	GalleryID uint   `gorm:"not null"`
	Filename  string `gorm:"not null "`
}

type ImageService interface {
	CreateImage(image *Image) error
	GetByImageIDU(id uint) ([]Image, error)
	ListImageMore(id uint) ([]Image, error)
	DeleteImageG(id uint) error
}

type ImageGorm struct {
	db *gorm.DB
}

func NewImageGorm(db *gorm.DB) ImageService {
	return &ImageGorm{db}
}

func (is *galleryGorm) ListImage(galleryid uint) ([]Image, error) {
	Images := []Image{}
	if err := is.db.Where("galleryid=?", galleryid).Find(&Images).Error; err != nil {
		return nil, err
	}

	return Images, nil
}

func (is *ImageGorm) CreateImage(image *Image) error {
	return is.db.Create(image).Error
}

func (is *ImageGorm) GetByImageIDU(id uint) ([]Image, error) {
	images := []Image{}
	err := is.db.
		Where("gallery_id = ?", id).
		Find(&images).Error
	if err != nil {
		return nil, err
	}
	return images, nil
}

func (is *ImageGorm) ListImageMore(id uint) ([]Image, error) {
	imageTable := []Image{}
	if err := is.db.Where("gallery_id=?", id).Find(&imageTable).Error; err != nil {
		return nil, err
	}
	return imageTable, nil
}

func (is *ImageGorm) DeleteImageG(id uint) error {
	ImageG := new(Image)
	err := is.db.First(ImageG, "id = ?", id).Find(&ImageG).Error
	if err != nil {
		return err
	}
	fmt.Println("id image", ImageG)
	err = os.RemoveAll(filepath.Join("upload", ImageG.Filename))
	if err != nil {
		log.Printf("Deleteting Not Success!!: %v\n", err)
	}
	// newDB := []string{}
	// for _, Image := range db {
	// 	if Image != ImageG {
	// 		newDB = append(newDB, image)
	// 	}
	// }
	// is.db = newDB
	return is.db.Where("id = ?", id).Delete(&Image{}).Error

}
