package models

import (
	"github.com/jinzhu/gorm"
)

type Gallery struct {
	gorm.Model
	Name       string `json:"name"`
	Userid     uint
	Status     bool
	Cover      string
	Desciption string `json:"desciption"`
	Active     uint   `json:"active"`
}
type GalleryDTO struct {
	ID         int
	Name       string `json:"name"`
	Userid     uint
	Status     bool
	Desciption string `json:"desciption"`
	Active     uint   `json:"active"`
}

// interface เอาไว้อ้างอิง เรียกใช้จริงๆไม่ได้ ๆต้องมีตัวอื่นมาทำงาน เป็นstruc
type GalleryService interface {
	Create(gallery *Gallery) error
	DeleteAlbum(id uint) error
	UpdateAlbum(gallery *Gallery) error
	ListAlbum(userid uint) ([]Gallery, error)
	GetGalleryByID(id uint) (*Gallery, error)
	ListAlubumMore() ([]Gallery, error)
	UpdateStatus(gallery *Gallery) error
	UpdateActive(gallery *Gallery) error
}

type galleryGorm struct {
	db *gorm.DB
}

func NewGalleryService(db *gorm.DB) GalleryService {
	return &galleryGorm{db}

}

func (gs *galleryGorm) Create(gallery *Gallery) error {
	return gs.db.Create(gallery).Error
}

func (gs *galleryGorm) ListAlbum(userid uint) ([]Gallery, error) {
	galleries := []Gallery{}
	if err := gs.db.Where("userid=?", userid).Find(&galleries).Error; err != nil {
		return nil, err
	}
	return galleries, nil
}

func (gs *galleryGorm) DeleteAlbum(id uint) error {
	data := new(Gallery)
	if err := gs.db.Where("id=?", id).First(data).Error; err != nil {
		return err
	}
	return gs.db.Delete(data).Error

}
func (gs *galleryGorm) GetGalleryByID(id uint) (*Gallery, error) {
	tt := new(Gallery)
	if err := gs.db.Where("id=?", id).First(tt).Error; err != nil {
		return nil, err
	}
	return tt, nil
}

func (gs *galleryGorm) UpdateAlbum(gallery *Gallery) error {
	data := new(Gallery)
	if err := gs.db.Where("id=?", gallery.ID).First(data).Error; err != nil {
		return err
	}
	if gallery.Name == "" {
		gallery.Name = data.Name
	}
	if gallery.Desciption == "" {
		gallery.Desciption = data.Desciption
	}
	return gs.db.Model(data).Updates(map[string]interface{}{"name": gallery.Name, "desciption": gallery.Desciption}).Error
}

func (gs *galleryGorm) ListAlubumMore() ([]Gallery, error) {
	galleryTable := []Gallery{}
	if err := gs.db.Where("status=?", true).Find(&galleryTable).Error; err != nil {
		return nil, err
	}
	return galleryTable, nil
}

func (gs *galleryGorm) UpdateStatus(gallery *Gallery) error {
	data := new(Gallery)
	if err := gs.db.Where("id=?", gallery.ID).First(data).Error; err != nil {
		return err
	}
	return gs.db.Model(data).Update("status", gallery.Status).Error
}

func (gs *galleryGorm) UpdateActive(gallery *Gallery) error {
	data := new(Gallery)
	if err := gs.db.Where("id=?", gallery.ID).First(data).Error; err != nil {
		return err
	}
	return gs.db.Model(data).Update("active", gallery.Active).Error
}
