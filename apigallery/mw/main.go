package mw

import (
	"apigallery/models"
	"fmt"

	"strings"

	"github.com/gin-gonic/gin"
)

func AuthRequired(gsu models.UserService) gin.HandlerFunc {
	return func(c *gin.Context) {
		header := c.GetHeader("Authorization")
		token := strings.TrimSpace(header[len("Bearer "):])
		if len(token) <= 7 {
			c.Status(401)
			c.Abort()
			return
		}
		if token == "" {
			c.Status(401)
			c.Abort()
			return
		}

		user, err := gsu.GetUserByToken(token)
		if err != nil {
			c.Status(401)
			c.Abort()
			return
		}
		_ = user
		c.Set("user", user)

		userLogin := c.Value("user").(*models.User)
		fmt.Printf("Check data User : %v", userLogin)
	}

}
