package handlers

import (
	"apigallery/models"
	"net/http"
	"strconv"

	"fmt"

	"github.com/gin-gonic/gin"
)

type CreateGallery struct {
	ID         uint
	Name       string `json:"name"`
	Userid     uint
	Status     bool
	Desciption string `json:"desciption"`
	Active     uint   `json:"active"`
}

type Galleryhandler struct {
	gs models.GalleryService
}

// add value in new table แล้วค่อยลงค่าทับ
func NewGalleryHandler(gs models.GalleryService) *Galleryhandler {
	return &Galleryhandler{gs}
}

func (gh *Galleryhandler) CreateGallery(c *gin.Context) {
	data := new(CreateGallery)
	if err := c.BindJSON(&data); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	userLogin := c.Value("user").(*models.User)
	fmt.Println("userData", userLogin.ID)

	gallery := new(models.Gallery)
	gallery.Name = data.Name
	gallery.Userid = userLogin.ID
	gallery.Status = true
	if err := gh.gs.Create(gallery); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"id":   gallery.ID,
		"name": gallery.Name,
	})

}
func (gh *Galleryhandler) ListAlbumUser(c *gin.Context) {
	userLogin := c.Value("user").(*models.User)
	fmt.Printf("get id User%v", userLogin)
	data, err := gh.gs.ListAlbum(uint(userLogin.ID))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	gallery := []models.GalleryDTO{}
	for _, a := range data {
		gallery = append(gallery, models.GalleryDTO{
			ID:         int(a.ID),
			Name:       a.Name,
			Userid:     uint(a.Userid),
			Status:     a.Status,
			Desciption: a.Desciption,
		})
	}
	c.JSON(http.StatusOK, gallery)
}

func (gh *Galleryhandler) DeleteAlbum(c *gin.Context) {
	idStr := c.Param("id")
	id, err := strconv.Atoi(idStr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	if err := gh.gs.DeleteAlbum(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(204)
}

func (gh *Galleryhandler) UpdateAlbum(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("min %v", id)

	album := new(models.Gallery)
	if err := c.BindJSON(album); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("alubum %v", album)

	found, err := gh.gs.GetGalleryByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	found.Name = album.Name
	found.Desciption = album.Desciption
	if err := gh.gs.UpdateAlbum(found); err != nil {
		c.JSON(500, gin.H{
			"maessage": err.Error(),
		})
		return
	}
	c.Status(204)

}
func (gh *Galleryhandler) ListAlbumAll(c *gin.Context) {
	album, err := gh.gs.ListAlubumMore()
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	albums := []CreateGallery{}
	for _, gallery := range album {
		albums = append(albums, CreateGallery{
			ID:     gallery.ID,
			Name:   gallery.Name,
			Userid: gallery.Userid,
			Active: gallery.Active,
		})
	}
	c.JSON(200, albums)
}

func (gh *Galleryhandler) UpdateStatusAlbum(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("id user %v", id)

	album := new(models.Gallery)
	if err := c.BindJSON(album); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("get value album %v", album)

	found, err := gh.gs.GetGalleryByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	found.Status = album.Status
	if err := gh.gs.UpdateStatus(found); err != nil {
		c.JSON(500, gin.H{
			"maessage": err.Error(),
		})
		return
	}
	c.Status(204)

}

func (gh *Galleryhandler) ActiveAlbum(c *gin.Context) {
	idstr := c.Param("id")
	id, err := strconv.Atoi(idstr)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("min %v", id)

	album := new(models.Gallery)
	if err := c.BindJSON(album); err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Printf("alubum %v", album)

	found, err := gh.gs.GetGalleryByID(uint(id))
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	found.Active = album.Active
	if err := gh.gs.UpdateActive(found); err != nil {
		c.JSON(500, gin.H{
			"maessage": err.Error(),
		})
		return
	}
	c.Status(204)

}
