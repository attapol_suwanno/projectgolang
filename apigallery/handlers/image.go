package handlers

import (
	"apigallery/models"
	"fmt"
	"log"
	"net/http"
	"path"
	"path/filepath"
	"strconv"

	"os"

	"github.com/gin-gonic/gin"
)

var db []string

type imageModel struct {
	ID        uint
	GalleryID uint   `gorm:"not null"`
	Filename  string `gorm:"not null "`
}

type ImageHandler struct {
	is models.ImageService
}

type ImageJSON struct {
	ID        uint `json:"id"`
	GalleryID uint
	Path      string `json:"path"`
}

func NewImageHandler(im models.ImageService) *ImageHandler {
	return &ImageHandler{im}
}

func (ih *ImageHandler) UploadImUser(c *gin.Context) {
	idStr := c.Param("gallery_id")
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	albumid := uint(id)

	err = os.MkdirAll("upload", os.ModePerm)
	if err != nil {
		c.Status(500)
		return
	}

	fmt.Printf("test err : %v", err)
	form, err := c.MultipartForm()
	if err != nil {
		log.Println(err)
		c.Status(400)
		return
	}
	files := form.File["photos"]
	images := []ImageJSON{}
	for _, file := range files {
		dst := filepath.Join("upload", file.Filename)
		err := c.SaveUploadedFile(file, dst)
		if err != nil {
			log.Println(err)
			c.Status(500)
			return
		}

		image := new(models.Image)
		image.GalleryID = albumid
		image.Filename = file.Filename
		if err = ih.is.CreateImage(image); err != nil {
			c.JSON(500, gin.H{
				"message": err.Error(),
			})
			return
		}

		db = append(db, file.Filename)
		images = append(images, ImageJSON{
			ID:   image.ID,
			Path: path.Join("/images", file.Filename),
		})
	}

	c.JSON(201, images)
}

func (ih *ImageHandler) ListImageAlbum(c *gin.Context) {
	idStr := c.Param("gallery_id")
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}

	fmt.Printf("get id gall>>>>>>>>>>>%v", id)
	images, err := ih.is.GetByImageIDU(uint(id))
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	res := []ImageJSON{}
	for _, img := range images {
		r := ImageJSON{}
		r.ID = img.ID
		r.GalleryID = uint(id)
		r.Path = path.Join("/images", img.Filename)
		res = append(res, r)
	}
	c.JSON(http.StatusOK, res)

}

func (ih *ImageHandler) ListImageAll(c *gin.Context) {
	idStr := c.Param("gallery_id")
	id, err := strconv.ParseInt(idStr, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	albumid := uint(id)

	imag, err := ih.is.ListImageMore(albumid)
	if err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}

	images := []imageModel{}
	for _, ima := range imag {
		images = append(images, imageModel{
			ID:        ima.ID,
			GalleryID: ima.GalleryID,
			Filename:  filepath.Join("/images", ima.Filename),
		})
	}
	c.JSON(200, images)
}

func (ih *ImageHandler) DeleteImage(c *gin.Context) {
	imageId := c.Param("id")
	id, err := strconv.ParseInt(imageId, 10, 32)
	if err != nil {
		c.JSON(400, gin.H{
			"message": err.Error(),
		})
		return
	}
	fmt.Println("id image", id)
	if err := ih.is.DeleteImageG(uint(id)); err != nil {
		c.JSON(500, gin.H{
			"message": err.Error(),
		})
		return
	}
	c.Status(http.StatusOK)
}
