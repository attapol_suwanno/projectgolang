package handlers

import (
	"apigallery/models"

	"fmt"

	"github.com/gin-gonic/gin"
)

type Userhandler struct {
	gsu models.UserService
}

type UserReq struct {
	Email    string
	Password string
	Name     string
}

func NewServerHandler(gsu models.UserService) *Userhandler {
	return &Userhandler{gsu}
}

func (ghu *Userhandler) UserSignup(c *gin.Context) {

	var req = new(UserReq)
	if err := c.BindJSON(req); err != nil {
		c.Status(400)
		return
	}
	user := new(models.User)
	user.Email = req.Email
	user.Password = req.Password
	user.Name = req.Name
	if err := ghu.gsu.UserSignup(user); err != nil {
		c.JSON(401, gin.H{
			"massage": "User already exits",
		})
		return
	}
	c.JSON(201, gin.H{
		"token": user.Token,
	})
}

func (ghu *Userhandler) UserLogin(c *gin.Context) {
	var login = new(models.User)
	if err := c.BindJSON(login); err != nil {
		c.Status(400)
		return
	}
	fmt.Printf("login >>> :%v", login)

	user := new(models.User)
	user.Email = login.Email
	user.Password = login.Password
	token, err := ghu.gsu.UserLoginDB(user)
	if err != nil {
		c.JSON(401, gin.H{
			"massage": err.Error(),
		})
		return
	}
	c.JSON(201, gin.H{
		"token": token,
	})
}

func (ghu *Userhandler) UserLogOut(c *gin.Context) {

	userLogin := c.Value("user").(*models.User)
	fmt.Printf("userloginnnn3 : %v", userLogin)

	if err := ghu.gsu.UserLogOut(userLogin); err != nil {
		c.JSON(500, gin.H{
			"massage": err.Error(),
		})
		return
	}
	c.Status(200)
}

type Profile struct {
	Name    string
	Picture string
}

func (ghu *Userhandler) GetUserProfile(c *gin.Context) {

	userLogin := c.Value("user").(*models.User)
	fmt.Printf("userloginnnn3 : %v", userLogin)
	userPro := Profile{
		Name:    userLogin.Name,
		Picture: userLogin.Picture,
	}
	c.JSON(200, userPro)

}
