package main

import (
	"apigallery/config"
	"apigallery/handlers"
	"apigallery/models"
	"apigallery/mw"
	"log"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/static"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

func main() {
	conf := config.Load()
	db, err := gorm.Open("mysql", "root:password@tcp(127.0.0.1:3306)/gallerybd?parseTime=true")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	db.LogMode(true) // dev only!!!!

	if err := db.AutoMigrate(&models.Gallery{}, &models.User{}, &models.Image{}).Error; err != nil {
		log.Fatal(err)
	}
	r := gin.Default()
	//create in main sent to models package ชั้นในสุด เพื่อที่จะออกไปนอกสุด
	gs := models.NewGalleryService(db)
	gsu := models.NewUserService(db, conf.HMACKey)
	is := models.NewImageGorm(db)
	//create in main sent to hanler package
	ghu := handlers.NewServerHandler(gsu)
	gh := handlers.NewGalleryHandler(gs)
	ih := handlers.NewImageHandler(is)
	config := cors.DefaultConfig()
	config.AllowOrigins = []string{"http://localhost:3000"}
	config.AllowHeaders = []string{"Authorization", "Content-Type"}

	r.Use(cors.New(config))
	r.POST("/signup", ghu.UserSignup)
	r.POST("/login", ghu.UserLogin)
	r.PATCH("/active/:id", gh.ActiveAlbum)
	r.GET("/gallerieall/:gallery_id", ih.ListImageAll)
	r.GET("/gallerieall", gh.ListAlbumAll)
	r.Use(static.Serve("/images", static.LocalFile("./upload", true)))
	auth := r.Group("/auth")
	auth.Use(mw.AuthRequired(gsu))
	{
		// auth.GET("/session", func(c *gin.Context) {
		// 	user, ok := c.Value("user").(models.User)
		// 	if !ok {
		// 		c.JSON(401, gin.H{
		// 			"massege": "invalid token",
		// 		})
		// 		return
		// 	}
		// 	c.JSON(200, user)
		// })
		auth.GET("/profile", ghu.GetUserProfile)

		auth.PATCH("/logout", ghu.UserLogOut)

		auth.POST("/galleries", gh.CreateGallery)

		auth.GET("/galleries", gh.ListAlbumUser)

		auth.DELETE("/galleries/:id", gh.DeleteAlbum)

		auth.PATCH("/galleries/:id", gh.UpdateAlbum)

		auth.PATCH("/galleries/:id/status", gh.UpdateStatusAlbum)

		auth.POST("/galleries/:gallery_id/images", ih.UploadImUser)

		auth.GET("/galleries/:gallery_id/images", ih.ListImageAlbum)

		auth.DELETE("/images/:id", ih.DeleteImage)
	}

	r.Run()
}
