import React, { useState, useEffect } from 'react'
import { Avatar, message, Button, Col, Row, } from 'antd';
import { useHistory } from "react-router-dom";
import { PoweroffOutlined, UserOutlined } from '@ant-design/icons';
import { UserLogOut, GetProfile } from '../../api/Index'

const ButtonLogin = () => {
    let history = useHistory();
    const key = 'updatable';
    const token = localStorage.getItem("token")
    const [valueP, setValue] = useState({})

    useEffect(() => {
        ImageProfile()
    }, []);

    const LogOut = () => {
        message.loading({ content: 'Please wait..', key });
        UserLogOut()
            .then((response) => {
                console.log(response.data)
                localStorage.setItem("token", "")
                history.push("/")
                setTimeout(() => {
                    message.success({ content: 'Logout success!', key, duration: 2 });
                }, 800);
            })
            .catch((err) => console.log(err))
        localStorage.setItem("token", "")

    }

    const ImageProfile = () => {
        GetProfile()
            .then((response) => {
                console.log(response.data)
                setValue(response.data)
            })
            .catch((err) => console.log(err))
    }
    console.log('image', valueP)

    return (
        <div>
            {token === "" ?
                <Row >
                    <Col span={24}>
                        <Button
                            style={{ marginLeft: '80%', backgroundColor: 'transparent', color: "black", borderColor: 'black', height: 40, width: 100 }}
                            type="primary"
                            shape="round"
                            onClick={() => history.push('/login')}>login</Button>
                    </Col>
                </Row>
                :
                <Row >
                    <Col span={24} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                        <Button
                            style={{ marginLeft: '80%', backgroundColor: 'transparent', color: 'black', borderColor: 'black', height: 40, width: 100 }}
                            type="primary"
                            shape="round"
                            icon={<PoweroffOutlined />}
                            onClick={LogOut}
                        >
                            Logout
                    </Button>
                        <div onClick={() => history.push('/Profile')}>
                            {valueP.Picture === "" ?
                                <div >
                                    <Avatar style={{ marginLeft: '20%' }} size={50} icon={<UserOutlined />} />
                                </div>
                                :
                                <div >
                                    <Avatar style={{ marginLeft: '20%' }} size={50} icon={<UserOutlined />} src={"http://localhost:8080/images" + valueP.Picture} />
                                </div>
                            }
                        </div>
                    </Col>
                </Row>
            }
        </div>
    )
}
export default (ButtonLogin)