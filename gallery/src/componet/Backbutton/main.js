import React from 'react'
import { Col, Row, } from 'antd';
import { useHistory } from "react-router-dom";
import { LeftOutlined } from '@ant-design/icons';

const ButtonBack = () => {
    let history = useHistory();
    const token = localStorage.getItem("token")

    return (
        <div>
            {token === "" ?
                null
                :
                <Col span={24}>
                    <LeftOutlined
                        onClick={() => history.goBack()}
                        style={{ marginLeft: '4%', fontSize: '30px', color: 'black', }} />
                </Col>

            }
        </div>

    )
}
export default (ButtonBack)