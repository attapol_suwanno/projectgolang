import React from "react";
import { BrowserRouter, Route, Redirect } from "react-router-dom";
import Home from '../screens/Home/main'
import Create from '../screens/Create/main'
import Login from '../screens/Login/main'
import Register from '../screens/Register/main'
import Showgallery from '../screens/ShowGallery/main'
import ShowImage from '../screens/ShowImage/main'
import Profile from '../screens/Profile/main'

const PrivateRoute = ({ path, component: Component }) => (
    <Route path={path} render={(props) => {
        const token = localStorage.getItem('token')
        if (token !== "")
            return <Component {...props} />

        else
            return <Redirect to="/Login" />
    }} />
)

const Routes = () => {
    return (
        <BrowserRouter>
            <PrivateRoute path="/Home" component={Home} exact={true} />
            <PrivateRoute path="/Create/:id" component={Create} exact={true} />
            <PrivateRoute path="/Profile" component={Profile} exact={true} />
            <Route path="/Login" component={Login} exact={true} />
            <Route path="/Register" component={Register} exact={true} />
            <Route path="/" component={Showgallery} exact={true} />
            <Route path="/ShowImage/:id" component={ShowImage} exact={true} />
        </BrowserRouter>
    )
}
export default (Routes)