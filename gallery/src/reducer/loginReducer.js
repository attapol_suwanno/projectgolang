import { ADD_TOKEN } from '../action/loginActions'
import { REMOVE_TOKEN } from '../action/loginActions'

const innitalstate = {
  token: localStorage.getItem('token') || null

}
export const loginReducer = (state = innitalstate, action) => {
  switch (action.type) {
    case ADD_TOKEN:
      console.log("token in Reducer", action.addToken)
      return {
        token: action.addToken
      }
    case REMOVE_TOKEN:
      return {
        token: null
      }
    default:
      return state
  }
};
