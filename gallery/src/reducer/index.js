import { combineReducers } from 'redux'
// import todos from './todos'
// import visibilityFilter from './visibilityFilter'
import { loginReducer } from '../reducer/loginReducer'

export const rootReducer = combineReducers({

    login: loginReducer

})
