import React, { useState } from 'react'
import { Layout, Typography } from 'antd';
import { useHistory } from "react-router-dom";
import { Form, Input, Select, Row, Col, Button, Menu, message } from 'antd';
import { UserSignup } from '../../api/Index'
import { createFromIconfontCN, GoogleOutlined, CloseOutlined, QuestionCircleOutlined } from '@ant-design/icons';
import Navbar from '../NavBar/main'

const IconFont = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});
const { Option } = Select;
const { Text, Title, Paragraph } = Typography;
const { Header, Content, Footer } = Layout;
const formItemLayout = {
  labelCol: {
    xs: { span: 24 },
    sm: { span: 8 },
  },
  wrapperCol: {
    xs: { span: 24 },
    sm: { span: 16 },
  },
};
const tailFormItemLayout = {
  wrapperCol: {
    xs: {
      span: 24,
      offset: 0,
    },
    sm: {
      span: 16,
      offset: 8,
    },
  },
};

const Register = () => {

  const [form] = Form.useForm();
  const [UserLogIn, setUserLogin] = useState([])
  let history = useHistory();

  const onFinish = values => {
    console.log('datasig', values)
    UserSignup(values)
      .then((response) => {
        setUserLogin(response.data)
        history.push("/Login");
        message.success('This is a success signup');
      })
      .catch((err) => {
        message.error('The Email  is worng!!');
        console.log(err)
      })

  };
  const [autoCompleteResult, setAutoCompleteResult] = useState([]);

  const onWebsiteChange = value => {
    if (!value) {
      setAutoCompleteResult([]);
    } else {
      setAutoCompleteResult(['.com', '.org', '.net'].map(domain => `${value}${domain}`));
    }
  };

  const websiteOptions = autoCompleteResult.map(website => ({
    label: website,
    value: website,
  }));

  return (
    <Layout className="layout" style={{ height: 1000 }}>
      <Navbar />
      <Row >
        <Col span={24}>
          <CloseOutlined
            onClick={() => history.push("/")}
            style={{ marginLeft: '94%', fontSize: '30px', color: 'black' }} />
        </Col>
      </Row>
      <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
        <Menu mode="horizontal" defaultSelectedKeys={['2']} style={{ backgroundColor: 'transparent', borderColor: 'white' }} >
          <Menu.Item key="1" onClick={() => history.push("/Login")}>Login</Menu.Item>
          <Menu.Item key="2">SingUp</Menu.Item>
        </Menu>
      </div>
      <Content style={{ marginTop: '10%' }}>
        <Row style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
          <div >
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
              <Title style={{ color: 'black', fontSize: 50 }} >Sign Up</Title>
            </div>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
              <Text style={{ color: 'black', fontSize: 20 }} >Already have  The ME Gallery account? </Text>
              <a style={{ fontSize: 20 }} onClick={() => history.push("/Login")} > Login </a>
            </div>
          </div>
        </Row>
        <Row style={{ marginTop: '5%' }}>
          <Col span={24} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
            <Form
              {...formItemLayout}
              form={form}
              name="register"
              onFinish={onFinish}
              initialValues={{
                residence: ['zhejiang', 'hangzhou', 'xihu'],
                prefix: '86',
              }}
              scrollToFirstError
            >
              <Form.Item

                name="email"
                rules={[
                  {
                    type: 'email',
                    message: 'The input is not valid E-mail!',
                  },
                  {
                    required: true,
                    message: 'Please input your E-mail!',
                  },
                ]}
              >
                <Input
                  placeholder="E-mail"
                  style={{ color: 'black', borderColor: 'black', height: 40, width: 280, borderRadius: 25 }}
                />
              </Form.Item>
              <Form.Item
                name="name"

                rules={[{ required: true, message: 'Please input your name!', whitespace: true }]}
              >
                <Input
                  placeholder="name"
                  style={{ color: 'black', borderColor: 'black', height: 40, width: 280, borderRadius: 25 }} />
              </Form.Item>
              <Form.Item
                name="password"
                rules={[
                  {
                    required: true,
                    message: 'Please input your password!',
                  },
                ]}
                hasFeedback
              >
                <Input.Password
                  placeholder="Password"
                  style={{ color: 'black', borderColor: 'black', height: 40, width: 280, borderRadius: 25 }}
                />
              </Form.Item>
              <Form.Item
                name="confirm"
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please confirm your password!',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject('The two passwords that you entered do not match!');
                    },
                  }),
                ]}
              >
                <Input.Password
                  placeholder="Confirm Password"
                  style={{ color: 'black', borderColor: 'black', height: 40, width: 280, borderRadius: 25 }}
                />
              </Form.Item>
              <Form.Item {...tailFormItemLayout}>
                <Button type="primary" shape="round" htmlType="submit"
                  style={{ backgroundColor: 'transparent', color: 'black', borderColor: 'black', height: 40, width: 100 }}
                >
                  Register
        </Button>
              </Form.Item>
            </Form>
          </Col>
          {/* <Col span={12} style={{ paddingLeft: '10%', paddingRight: '10%' }}>
            <Row>
              <div style={{ backgroundColor: '#3b5998', width: 300, height: 45, border: '1.5px solid #3b5998' }}>
                <IconFont style={{ padding: 13.5, backgroundColor: 'white', color: '#4267B2', marginRight: '15%' }} type="icon-facebook" />
                <Text style={{ color: 'white', marginRight: '10%' }}>Continue with Facebook</Text>
              </div>
            </Row>
            <Row>
              <div style={{ borderWidth: 2, backgroundColor: '#4285F4', width: 300, height: 45, border: '1.8px solid #4285F4', marginTop: '5%' }}>
                <GoogleOutlined style={{ padding: 13.5, backgroundColor: 'white', marginRight: '15%' }} />
                <Text style={{ color: 'white', marginRight: '20%' }}>Continue with Google</Text>
              </div>
            </Row>
          </Col> */}
        </Row>
      </Content>
      <Footer style={{ textAlign: 'center' }}> Design ©2020 Created by Attapol Suwanno 602110181</Footer>
    </Layout>
  )

}
export default (Register)