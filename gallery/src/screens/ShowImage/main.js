import React, { useState, useEffect } from 'react'
import { Layout, Typography, Col, Row, Button, List, Card, Input, Badge, Form, Menu } from 'antd';
import { Link } from 'react-router-dom'
import { DownloadOutlined, LeftOutlined } from '@ant-design/icons';
import Carousel, { Modal, ModalGateway } from 'react-images';
import { ImageOfAlbum } from '../../api/Index'
import { useHistory } from "react-router-dom";
import logo from '../../assets/logoGallery-1.png'
import { useLocation } from 'react-router';
import ButtonLogin from '../../componet/ButtonLogin&Logout/main'

const { Text, Title, Paragraph } = Typography;
const { Search } = Input;
const { Header, Content, Footer } = Layout;
const { Meta } = Card;

const ShowImage = () => {
    const [previewVisible, setPreviewVisible] = useState(false)
    const [imageShow, setImageShow] = useState({})
    const [indexImag, setIndexImag] = useState()
    const [Gallery, setGallery] = useState([])

    const location = useLocation()
    let history = useHistory();
    const DataGellery = location.state.itemGallery
    const handleCancel = () => setPreviewVisible(false);

    console.log("data Gell test", DataGellery.ID)
    useEffect(() => {
        ImageOfAlbum(DataGellery.ID)
            .then((response) => {
                console.log(response.data)
                setGallery(response.data)
            })

            .catch((err) => console.log(err))
    }, []);

    useEffect(() => {
        setImageShow(Gallery.map((item) => {

            return {
                source: {
                    download: "http://localhost:8080" + item.Filename,
                    fullscreen: "http://localhost:8080" + item.Filename,
                    regular: "http://localhost:8080" + item.Filename,
                    thumbnail: "http://localhost:8080" + item.Filename,
                }
            }
        })
        )
    }, [Gallery]);

    const gridStyle = {
        width: 200,
        textAlign: 'center',
    };

    return (
        <Layout className="layout" >
            <Col spen={24}  >
                <div
                    style={{
                        backgroundImage: `url("https://d1gn9jfso7kpav.cloudfront.net/wp-content/uploads/2019/10/0037_2019_BM_Self-0223.jpg")`, backgroundRepeat: 'no-repeat', width: "100%", height: 850, color: 'white'
                    }}>
                    <Header style={{ backgroundColor: 'transparent' }}>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <img style={{ width: 400 }} src={logo} />
                        </div>
                    </Header>
                    <Row>
                        <Col span={10} style={{ marginTop: '2%' }}>
                            <LeftOutlined
                                onClick={() => history.goBack()}
                                style={{ marginLeft: '4%', fontSize: '30px', color: 'black', }} />
                        </Col>
                        <Col span={12}>
                            <ButtonLogin />
                        </Col>
                        <Col />
                    </Row>
                    <div style={{ marginLeft: '20%', marginRight: '20%', marginTop: '30%' }}>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Title style={{ color: 'white' }} >{"{WORLD GALLERY}"}</Title>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Title style={{ color: 'white', fontSize: 50 }} >P H O T O G R A P H Y</Title>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Paragraph style={{ color: 'white', fontSize: 18 }}>Everything you need for your photography website Stunning photography colection,dozens of beautiful galleries,select colors ans tons of stotage so you can upload worry free.</Paragraph>
                        </div>
                    </div>
                </div>
            </Col>
            <Content >
                <div>
                    <Col span={24}>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '3%' }}>
                            <Title>{`{'Album : ${DataGellery.name} '}`}</Title>
                        </div>
                        <Row style={{ margin: '5%', display: 'flex', alignItems: 'center', justifyContent: 'center', border: '5px solid white ', borderColor: 'white' }}>
                        {Gallery.map((item, index) => (
                            <Card.Grid style={{ display: "flex", flexFlow: "column wrap", width: "400px",}}  >
                                <div onClick={() => {
                                    setIndexImag(index)
                                    setPreviewVisible(true)
                                }}
                                >
                                        <div  style={{  display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                                            <img alt="example" src={"http://localhost:8080" + item.Filename} style={{ width: "95%", height: '90%' }} />
                                        </div>
                                </div>
                            </Card.Grid>
                        ))}
                        </Row>
                        <ModalGateway>
                            {previewVisible ? (
                                <Modal onClose={handleCancel}>
                                    <Carousel views={imageShow} currentIndex={indexImag} />
                                </Modal>
                            ) : null}
                        </ModalGateway>
                    </Col>
                </div>
            </Content>
            <Footer style={{ textAlign: 'center' }}> Design ©2020 Created by Attapol Suwanno 602110181</Footer>
        </Layout>
    )

}
export default (ShowImage)

