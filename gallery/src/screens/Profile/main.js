import React, { useState, useEffect } from 'react'
import { Layout, Menu, Breadcrumb, Typography, Col, Row, Carousel, } from 'antd';
import { LeftOutlined } from '@ant-design/icons';
import { useHistory } from "react-router-dom";
import { GetProfile } from '../../api/Index'
import logo from '../../assets/logoGallery-1.png'
import ButtonLogin from '../../componet/ButtonLogin&Logout/main'
const { Title } = Typography;
const { Header, Content, Footer } = Layout;

const Profile = () => {
    let history = useHistory();
    const [valueP, setValue] = useState({})
    console.log('test value', valueP)
    useEffect(() => {
        GetProfile()
            .then((response) => {
                console.log(response.data)
                setValue(response.data)
            })
            .catch((err) => console.log(err))
    }, []);

    return (
        <Layout className="layout">
            <Col spen={24}  >
                <Content style={{ height: 1100 }}>
                    <div
                        style={{
                            backgroundImage: `url("https://orensteinberg.com/wp-content/uploads/2019/05/samuel-zeller-34751-unsplash.jpg")`, backgroundRepeat: 'no-repeat', width: "100%", height: 1050, color: 'white'
                        }}>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <img style={{ width: 400 }} src={logo} onClick={() => history.push("/")} />
                        </div>
                        <Row>
                            <Col span={10}>
                                <LeftOutlined
                                    onClick={() => history.goBack()}
                                    style={{ marginLeft: '4%', fontSize: '30px', color: 'white' }} />
                            </Col>
                            <Col span={12}>
                                <ButtonLogin />
                            </Col>
                            <Col />
                        </Row>
                        <Col>
                            <Row span={24} >
                                <div style={{ backgroundColor: 'white', width: '40%', height: '30%', marginTop: '10%' }}>
                                    <div style={{ color: 'black', display: 'flex', alignItems: 'center', justifyContent: 'center', }} >
                                        <Title style={{ color: 'black' }} >Prifile</Title>
                                        <Title style={{ color: 'black', fontSize: 50 }} >{valueP.Name}</Title>
                                    </div>
                                    {valueP.Picture === "" ?
                                        <div >
                                            <img src={"https://images.unsplash.com/photo-1499482125586-91609c0b5fd4?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=700&q=60"} style={{ width: '60%', height: "30%", marginLeft: '55%' }} />
                                        </div>
                                        :
                                        <div >
                                            <img src={"http://localhost:8080/images" + valueP.Picture} style={{ width: '75%', height: "80%", marginLeft: '55%' }} />
                                        </div>
                                    }
                                </div>
                                <Col style={{ width: '40%', height: 400, marginLeft: '15%', marginTop: '5%' }} >
                                    <Carousel autoplay  >
                                        <div>
                                            <h3>
                                                <img style={{ width: '100%' }} src={"https://cdn.stocksnap.io/img-thumbs/960w/buildings-architecture_S11G2VLMOU.jpg"} />

                                            </h3>
                                        </div>
                                        <div>
                                            <h3>
                                                <img style={{ width: '100%' }} src={"https://i.pinimg.com/564x/41/00/b8/4100b8ccb296d1e23343a73793a5d6fe.jpg"} />
                                            </h3>
                                        </div>
                                        <div>
                                            <h3>
                                                <img style={{ width: '100%' }} src={"https://i.pinimg.com/564x/0c/68/ac/0c68ac6e83003f90e84da734609d2105.jpg"} />
                                            </h3>
                                        </div>

                                        <div>
                                            <h3>
                                                <img style={{ width: '100%' }} src={"https://www.clickinmoms.com/blog/wp-content/uploads/2015/11/10-question-interview-with-photographer-Tiffany-Kelly-2.jpg"} />

                                            </h3>
                                        </div>
                                    </Carousel>
                                </Col>
                            </Row>
                        </Col>
                    </div>
                </Content>
            </Col>

            <Footer style={{ textAlign: 'center' }}>Ant Design ©2018 Created by Ant UED</Footer>
        </Layout>
    )
}
export default Profile
