import React, { useState, useEffect } from 'react'
import { Layout, Switch, Popconfirm, Col, Row, Button, List, Card, Input, Form, Badge, Modal, Typography } from 'antd';
import { QuestionCircleOutlined } from '@ant-design/icons';
import { createAlbum } from '../../api/Index';
import { listAlbum } from '../../api/Index'
import { DeleteAlbum, UpdateNameAlbum, UpdateStatusAlbum, listImage } from '../../api/Index'
import { CloseOutlined, CheckOutlined, EditOutlined } from '@ant-design/icons'
import { connect } from 'react-redux';
import Navbar from '../NavBar/main'
import { useHistory, Link } from "react-router-dom";
import ButtonLogin from '../../componet/ButtonLogin&Logout/main'
const { Header, Content, Footer } = Layout;
const { Meta } = Card;
const { Title } = Typography;
const { TextArea } = Input;

const Home = (props) => {
    const [GalleryName, setGalleryName] = useState([])
    const [visible, setVisible] = useState(false)
    const [CartEditID, setCartEditID] = useState()
    const [defaultNameCart, setdefaultNameCart] = useState("")
    const [defaultDesCart, setDefaultDesCart] = useState("")
    let history = useHistory();

    const token = localStorage.getItem("token")

    useEffect(() => {
        fetch()
    }, []);

    const fetch = () => {
        listAlbum(token)
            .then((response) => {
                console.log('0000000000', response.data)
                setGalleryName(response.data)
            })
            .catch((err) => console.log(err))
    }


    const gridStyle = {
        width: 280,
        textAlign: 'center',
    };

    const onFinish = (values) => {
        createAlbum(values)
            .then((response) => {
                console.log(response.data)
                window.location.reload()
            })
            .catch((err) => console.log(err))
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    const onFinishEdit = (values) => {
        console.log('value', values)
        UpdateNameAlbum(values, CartEditID)
            .then((response) => {
                console.log(response.data)
                window.location.reload()
            })
            .catch((err) => console.log(err))
    };

    const onFinishFailedEdit = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    const onHover = (item) => {
        DeleteAlbum(item)
            .then((response) => {
                console.log(response.data)
                window.location.reload()
            })
            .catch((err) => console.log(err))
    }
    const handleClick = (item, index) => {
        var indexpage = index + 1
        var IdAlum = item.ID
        history.push("/create/" + indexpage, { album: IdAlum })
    }
    const showModal = (item) => {
        console.log("Id cart", item.ID)
        console.log("name cart", item)
        setCartEditID(item.ID)
        setdefaultNameCart(item.name)
        setDefaultDesCart(item.desciption)
        setVisible(true)
    };

    const handleOk = e => {
        setVisible(false)
    };

    const handleCancel = e => {
        setVisible(false)
    };

    const onChange = (value, item) => {
        UpdateStatusAlbum(value, item)
            .then((response) => {
                console.log(response.data)
            })
            .catch((err) => console.log(err))
    }

    return (
        <Layout className="layout" style={{ height: 1000 }}>
            <Navbar />
            <ButtonLogin />
            {
                GalleryName == [] ?
                    null
                    :
                    <Content style={{ marginTop: '5%', marginLeft: '5%', marginRight: '5%', }}>
                        <Row style={{ marginLeft: '20%', marginRight: '30%', }}>
                            <Col span={24} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                                <Form
                                    name="basic"
                                    onFinish={onFinish}
                                    onFinishFailed={onFinishFailed}
                                >
                                    <Row>
                                        <Col>
                                            <Form.Item
                                                name="name"
                                                rules={[{ required: true, message: 'Please input name gellery!!' }]}
                                            >
                                                <Input
                                                    placeholder="Input to add new gallery"
                                                    style={{ color: 'black', borderColor: 'black', height: 40, width: 220, borderRadius: 25 }}
                                                />
                                            </Form.Item>
                                        </Col>
                                        <Col>
                                            <Form.Item >

                                                <Button
                                                    style={{ marginLeft: '30%', backgroundColor: 'transparent', color: 'black', borderColor: 'black', height: 40, width: 100 }}
                                                    type="primary"
                                                    shape="round"
                                                    htmlType="submit">
                                                    Add
                                                </Button>
                                            </Form.Item>
                                        </Col>
                                    </Row>
                                </Form>
                            </Col>
                        </Row>
                        <Modal
                            title="Edite Name Gellery"
                            visible={visible}
                            onOk={handleOk}
                            footer={false}
                            onCancel={handleCancel}
                        >
                            <Col spen={24} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                                <Form
                                    name="basic"
                                    onFinish={onFinishEdit}
                                    onFinishFailed={onFinishFailedEdit}
                                >   <Title level={4}>Name gallery</Title>
                                    <Form.Item
                                        name="name"
                                        initialValues={defaultNameCart}
                                    >
                                        <Input
                                            defaultValue={defaultNameCart}
                                            style={{ color: 'black', borderColor: 'black', height: 40, width: 300, borderRadius: 25 }}
                                        />
                                    </Form.Item>
                                    <Title level={4}>Desciption</Title>
                                    <Form.Item
                                        name="desciption"
                                        initialValues={defaultDesCart}
                                    >
                                        <TextArea 
                                             rows={4} 
                                            defaultValue={defaultDesCart}
                                            style={{ color: 'black', borderColor: 'black', height: '100%', width: 300, borderRadius: 25 }}
                                        />
                                    </Form.Item>
                                    <Form.Item >
                                        <Button
                                            style={{ marginLeft: '30%', backgroundColor: 'transparent', color: 'black', borderColor: 'black', height: 40, width: 100 }}
                                            type="primary"
                                            shape="round"
                                            htmlType="submit"
                                        >
                                            Edit
                                        </Button>
                                    </Form.Item>
                                </Form>
                            </Col>
                        </Modal>
                        <Col span={24}>
                            <List
                                grid={{
                                    gutter: 16,
                                    xs: 1,
                                    sm: 2,
                                    md: 4,
                                    lg: 4,
                                    xl: 6,
                                    xxl: 3,
                                }}
                                dataSource={GalleryName}
                                renderItem={(item, index) => (
                                    <Card.Grid style={gridStyle} style={{ padding: '1%' }}>
                                        <div style={{ marginLeft: '95%' }}>
                                            <Popconfirm title="Are you sure？" icon={<QuestionCircleOutlined style={{ color: 'red' }} />}>
                                                <Badge onClick={() => onHover(item)}  >
                                                    <CloseOutlined style={{ padding: 5, backgroundColor: 'red', color: 'white', borderRadius: 20 }} />
                                                </Badge>
                                            </Popconfirm>
                                            <EditOutlined
                                                className="EditOutlined"
                                                style={{ fontSize: "20px", marginTop: '50%' }}
                                                onClick={() =>
                                                    showModal(item)}
                                            />
                                        </div>
                                        <div>
                                            <Switch
                                                checkedChildren={<CheckOutlined />}
                                                unCheckedChildren={<CloseOutlined />}
                                                defaultChecked={item.Status}
                                                onChange={(value) => onChange(value, item)}
                                            />
                                        </div>
                                        <div onClick={() => handleClick(item, index)}>
                                            <Row>
                                                <Col span={12}>
                                                    <Card
                                                        style={{ width: 150, height: 150, }}
                                                    >
                                                        <Meta title={item.name} description={item.desciption} />
                                                    </Card>
                                                </Col>
                                                <Col span={12}>
                                                    <Card
                                                        style={{ width: 150, height: 150 }}
                                                        cover={<img alt="example" src="https://cdn.pixabay.com/photo/2018/12/08/16/56/slr-3863649_960_720.jpg" style={{ width: 150, height: 150 }} />}
                                                    >
                                                    </Card>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Card.Grid>
                                )}
                            />
                        </Col>
                    </Content>
            }

            <Footer style={{ textAlign: 'center' }}> Design ©2020 Created by Attapol Suwanno 602110181</Footer>
        </Layout>
    )
}

const mapStateToProps = state => {
    return {
        tokenUser: state.login.token,
    }
}


export default connect(mapStateToProps, null)(Home)

