import React, { useState } from 'react'
import { Form, Input, Button, Typography, Col, Row, Layout, Menu, message, } from 'antd';
import { UserOutlined, LockOutlined, createFromIconfontCN, CloseOutlined } from '@ant-design/icons';
import { connect } from 'react-redux';
import { UserLogin } from '../../api/Index'
import { addToken } from '../../action/loginActions'
import Navbar from '../NavBar/main'
import { useHistory } from "react-router-dom";
const { Text, Title } = Typography;
const { Content, Footer } = Layout;

const IconFont = createFromIconfontCN({
    scriptUrl: '//at.alicdn.com/t/font_8d5l8fzk5b87iudi.js',
});
const Login = (props) => {
    const [id, setId] = useState()
    let history = useHistory();
    const key = 'updatable';

    const onFinish = values => {
        UserLogin(values)
            .then((response) => {
                console.log("Login res", response)
                localStorage.setItem("token", response.data.token)
                localStorage.setItem("userid", response.data.id)
                message.loading({ content: 'Loading...', key });
                setTimeout(() => {
                    message.success({ content: 'Login success!', key, duration: 2 });
                }, 800);
                setId(response.data.id)
                history.push("/Home");
            })
            .catch((err) => {
                message.error('Please check the Email and Password try again!!!');
                alert(err)
            })
    };
    return (
        <Layout className="layout" style={{ height: 1100 }} >
            <Navbar />
            <Row >
                <Col span={24}>
                    <CloseOutlined
                        onClick={() => history.push("/")}
                        style={{ marginLeft: '94%', fontSize: '30px', color: 'black' }} />
                </Col>
            </Row>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
                <Menu mode="horizontal" defaultSelectedKeys={['1']} style={{ backgroundColor: 'transparent', borderColor: 'white' }} >
                    <Menu.Item key="1" >Login</Menu.Item>
                    <Menu.Item key="2" onClick={() => history.push("/Register")}>SingUp</Menu.Item>
                </Menu>
            </div>
            <Content style={{ marginTop: '10%' }} >
                <Row style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                    <div >
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Title style={{ color: 'black', fontSize: 50 }} >Login</Title>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Text style={{ color: 'black', fontSize: 20 }} >Next to The ME Gallery  </Text>
                            <a style={{ fontSize: 20 }} onClick={() => history.push("/Register")} >  Sign Up </a>
                        </div>
                    </div>
                </Row>
                <Row style={{ marginTop: '5%' }}>
                    <Col span={24} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                        <Form
                            name="normal_login"
                            className="login-form"
                            initialValues={{ remember: true }}
                            onFinish={onFinish}
                        >
                            <Form.Item
                                name="email"
                                rules={[{ required: true, message: 'Please input your Username!' }]}
                            >
                                <Input
                                    style={{ color: 'black', borderColor: 'black', height: 40, width: 280, borderRadius: 25 }}
                                    prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Username" />
                            </Form.Item>
                            <Form.Item
                                name="password"
                                rules={[{ required: true, message: 'Please input your Password!' }]}
                            >
                                <Input
                                    style={{ color: 'black', borderColor: 'black', height: 40, width: 280, borderRadius: 25 }}
                                    prefix={<LockOutlined className="site-form-item-icon" />}
                                    type="password"
                                    placeholder="Password"
                                />
                            </Form.Item>
                            <Form.Item>
                                <Button type="primary" shape="round" htmlType="submit" className="login-form-button"
                                    style={{ backgroundColor: 'transparent', color: 'black', borderColor: 'black', height: 40, width: 100 }}
                                >
                                    Log in
                            </Button>
                                Or <a onClick={() => history.push("/Register")} >register now!</a>
                            </Form.Item>
                        </Form>
                    </Col>
                </Row>
            </Content>
            <Footer style={{ textAlign: 'center' }}> Design ©2020 Created by Attapol Suwanno 602110181</Footer>
        </Layout>

    )
}


const mapDispatchToProps = {
    addToken
}

export default connect(null, mapDispatchToProps)(Login)


