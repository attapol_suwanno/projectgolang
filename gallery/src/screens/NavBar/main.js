import React from 'react'
import { Layout } from 'antd';
import { useHistory } from "react-router-dom";
import logo from '../../assets/logoGallery-2.png'
const { Header } = Layout;

const NavBar = () => {
    let history = useHistory();
    return (
        <Header style={{ backgroundColor: 'transparent' }}>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                <img style={{ width: 300 }} src={logo} onClick={() => history.push("/")} />
            </div>
        </Header>
    )
}
export default NavBar