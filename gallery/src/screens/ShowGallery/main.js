import React, { useState, useEffect } from 'react'
import { Layout, Typography, Col, Row, Button, List, Card, Input, Carousel, Empty, BackTop, Statistic, message, Space } from 'antd';
import { listAlbumAll, ImageOfAlbum, ActiveView } from '../../api/Index'
import { EditOutlined } from '@ant-design/icons'
import { useHistory } from "react-router-dom";
import ButtonLogin from '../../componet/ButtonLogin&Logout/main'
import ButtonBack from '../../componet/Backbutton/main'
import logo from '../../assets/logoGallery-1.png'
const { Text, Title, Paragraph } = Typography;
const { Search } = Input;
const { Header, Content, Footer } = Layout;
const { Meta } = Card;

const Showgallery = () => {
    const [GalleryName, setGalleryName] = useState([])
    const [SearchValue, setSearchValue] = useState(null)
    const token = localStorage.getItem("token")
    let history = useHistory();

    useEffect(() => {
        listAlbumAll()
            .then((response) => setGalleryName(response.data))

            .catch((err) => console.log(err))
    }, []);

    const gridStyle = {
        width: 280,
        textAlign: 'center',
    };

    const ActiveFucn = (item) => {
        const count = item.active + 1
        const id = item.ID
        console.log('count :', count)
        console.log('Id :', id)

        ActiveView(id, count)
            .then((response) => console.log(response.data))
            .catch((err) => console.log(err))
    }

    const onClickAlbum = (item) => {
        console.log("tes1", item)
        var pageImage = item.ID
        ImageOfAlbum(item.ID)
            .then((response) => {
                (response.data.length == 0 ?
                    message.warning('Album no Image !!!')
                    :
                    history.push("/ShowImage/" + pageImage, { itemGallery: item })
                )
            })
            .catch((err) => console.log(err))
    }

    const SearchName = event => {
        console.log('even ', event)
        let Keyword = event.target.value
        setSearchValue(Keyword)
    }

    return (
        <Layout className="layout">
            <Col spen={24}  >
                <div
                    style={{
                        backgroundImage: `url("https://d1gn9jfso7kpav.cloudfront.net/wp-content/uploads/2019/10/0037_2019_BM_Self-0223.jpg")`, backgroundRepeat: 'no-repeat', width: "100%", height: 850, color: 'white'
                    }}>
                    <Header style={{ backgroundColor: 'transparent' }}>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <img style={{ width: 400 }} src={logo} />
                        </div>
                    </Header>
                    <Row>
                        <Col span={10} style={{ marginTop: '2%' }}>
                            <ButtonBack />

                        </Col>
                        <Col span={12}>
                            <ButtonLogin />
                        </Col>
                        <Col />
                    </Row>
                    <div style={{ marginLeft: '20%', marginRight: '20%', marginTop: '30%' }}>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Title style={{ color: 'white' }} >{"{WORLD GALLERY}"}</Title>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Title style={{ color: 'white', fontSize: 50 }} >P H O T O G R A P H Y</Title>
                        </div>
                        <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                            <Paragraph style={{ color: 'white', fontSize: 18 }}>Everything you need for your photography website Stunning photography colection,dozens of beautiful galleries,select colors ans tons of stotage so you can upload worry free.</Paragraph>
                        </div>
                    </div>
                </div>
            </Col>
            <Content >
                {GalleryName.length == 0 ?
                    <div>
                        <Col span={24} style={{ margin: '5%' }}>
                            <Empty />
                        </Col>
                        <Col span={24} >
                            <Carousel autoplay  >
                                <div>
                                    <h3>
                                        <img style={{ width: '100%' }} src={"https://cdn.stocksnap.io/img-thumbs/960w/buildings-architecture_S11G2VLMOU.jpg"} />

                                    </h3>
                                </div>
                                <div>
                                    <h3>
                                        <img style={{ width: '100%' }} src={"https://i.pinimg.com/564x/41/00/b8/4100b8ccb296d1e23343a73793a5d6fe.jpg"} />
                                    </h3>
                                </div>
                                <div>
                                    <h3>
                                        <img style={{ width: '100%' }} src={"https://i.pinimg.com/564x/0c/68/ac/0c68ac6e83003f90e84da734609d2105.jpg"} />
                                    </h3>
                                </div>

                                <div>
                                    <h3>
                                        <img style={{ width: '100%' }} src={"https://www.clickinmoms.com/blog/wp-content/uploads/2015/11/10-question-interview-with-photographer-Tiffany-Kelly-2.jpg"} />

                                    </h3>
                                </div>
                            </Carousel>
                        </Col>
                    </div>
                    :

                    <div>
                        <Col spen={12} style={{ backgroundColor: 'black', paddingTop: '4%', paddingBottom: '4%' }}>
                            <div style={{ marginLeft: '20%', marginRight: '20%', }}>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                                    <Title level={2} style={{ color: 'white', }}>SEARCH  OUR  PICTURE</Title>
                                </div>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                                    <Search
                                        placeholder=" search "
                                        onChange={(e) => SearchName(e)}
                                        style={{ color: 'white', borderColor: 'white', height: 40, width: 280, borderRadius: 25 }}
                                    />
                                </div>
                            </div>
                        </Col>

                        {SearchValue !== null ?
                            <Col span={24}>
                                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '3%' }}>
                                    <Title>GALLERY</Title>
                                </div>
                                {GalleryName.filter((data) => {
                                    if (SearchValue == null)
                                        return data

                                    else if (data.name.toLowerCase().includes(SearchValue.toLowerCase())) {
                                        return data
                                    }
                                }).map(data => {
                                    return (
                                        <Card.Grid style={gridStyle} style={{ padding: '2%' }} onClick={() => {
                                            onClickAlbum(data)
                                            ActiveFucn(data)
                                        }}>

                                            <Row>
                                                <Col span={12}>
                                                    <Card
                                                        style={{ width: 150, height: 150, }}
                                                    >
                                                        <Meta title={data.name} />
                                                    </Card>
                                                </Col>
                                                <Col span={12}>
                                                    <Card
                                                        style={{ width: 150, height: 150 }}
                                                        cover={<img alt="example" src="https://cdn.pixabay.com/photo/2018/12/08/16/56/slr-3863649_960_720.jpg" style={{ width: 150, height: 150 }} />}
                                                    >
                                                    </Card>
                                                </Col>
                                                <Col span={12}>
                                                    <Statistic title="VIEWS" value={data.active} />
                                                </Col>
                                            </Row>
                                        </Card.Grid>
                                    )
                                })
                                }
                            </Col>
                            :
                            <Col span={24}>
                                <Row>
                                    <Col span={20} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '3%' }}>
                                        <Title style={{ marginLeft: '18%', }}>GALLERY</Title>
                                    </Col>
                                    <Col span={4} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '3%' }}>
                                        {token === "" ?
                                            null
                                            :
                                            <EditOutlined
                                                className="EditOutlined"
                                                style={{ fontSize: "30px" }}
                                                onClick={() => history.push("/Home")}
                                            />
                                        }
                                    </Col>
                                </Row>
                                <List
                                    grid={{
                                        gutter: 16,
                                        xs: 1,
                                        sm: 2,
                                        md: 4,
                                        lg: 4,
                                        xl: 6,
                                        xxl: 3,
                                    }}
                                    dataSource={GalleryName}
                                    renderItem={(item, Index) => (
                                        <Card.Grid style={gridStyle} style={{ padding: '2%' }} onClick={() => {
                                            onClickAlbum(item, Index)
                                            ActiveFucn(item)
                                        }}>
                                            <Row>
                                                <Col span={12}>
                                                    <Card
                                                        style={{ width: 150, height: 150, }}
                                                    >
                                                        <Meta title={item.name} />
                                                    </Card>
                                                </Col>
                                                <Col span={12}>
                                                    <Card
                                                        style={{ width: 150, height: 150 }}
                                                        cover={<img alt="example" src="https://cdn.pixabay.com/photo/2018/12/08/16/56/slr-3863649_960_720.jpg" style={{ width: 150, height: 150 }} />}
                                                    >
                                                    </Card>
                                                </Col>
                                            </Row>
                                            <Col span={12}>
                                                <Statistic title="VIEWS" value={item.active} />
                                            </Col>
                                        </Card.Grid>
                                    )}
                                />
                            </Col>
                        }
                    </div>
                }
                <BackTop />
            </Content>
            <Footer style={{ textAlign: 'center' }}> Design ©2020 Created by Attapol Suwanno 602110181</Footer>
        </Layout>
    )

}


export default (Showgallery)

