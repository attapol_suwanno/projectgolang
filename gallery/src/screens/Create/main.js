import React, { useState, useEffect } from 'react'
import { Layout, Badge, Popconfirm, Col, Row, Button, Card, Upload, message, Empty, Typography, notification, Space } from 'antd';
import { PlusOutlined } from '@ant-design/icons';
import { LeftOutlined, CloseOutlined } from '@ant-design/icons';
import { UploadImage, listImage, DeleteImage } from '../../api/Index'
import Navbar from '../NavBar/main'
import { UploadOutlined } from '@ant-design/icons';
import { useLocation } from 'react-router';
import { useHistory } from "react-router-dom";
import ButtonLogin from '../../componet/ButtonLogin&Logout/main'
const { Header, Content, Footer } = Layout;
const { Text, Title, Paragraph } = Typography;

function getBase64(file) {
  return new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => resolve(reader.result);
    reader.onerror = error => reject(error);
  });
}

const Create = () => {
  const [previewVisible, setPreviewVisible] = useState(false)
  const [antimage, setantimage] = useState([])
  let history = useHistory();
  const [fileList, setFileList] = useState([])
  const [imageall, setImage] = useState([])
  const location = useLocation()
  const IdAlum = location.state.album

  useEffect(() => {
    console.log("test alum ID", IdAlum)
    listImage(IdAlum)
      .then((res) => {
        console.log("res", res.data)
        setImage(res.data)
        console.log(res.message);
      })
      .catch((err) => console.error(err))

  }, []);

  const handleCancel = () => setPreviewVisible(false);

  const onClickUpload = () => {
    var form = new FormData();
    for (let i = 0; i < antimage.length; i++) {
      form.append("photos", antimage[i]);
    }

    UploadImage(form, IdAlum)
      .then((res) => {
        console.log(res.message);
        window.location.reload()
      })
      .catch((err) => console.error(err))
  }

  const gridStyle = {
    textAlign: 'center',
  };

  const uploadButton = (
    <div>
      <PlusOutlined />
      <div className="ant-upload-text">Add image</div>
    </div>
  )

  const onClickDeleteImg = (item) => {
    console.log('id img', item.id)
    DeleteImage(item.id)
      .then((res) => {
        console.log(res.message);
        window.location.reload()
      })
      .catch((err) => console.error(err))
    console.log('test', item)
  }

  return (
    <Layout className="layout">
      <Col spen={24}  >
        <div
          style={{
            backgroundImage: `url("https://static1.squarespace.com/static/598ac373e4fcb565bf90686f/598ac476f5e23155afc7ed18/5e372c4d26acc86be0fe23a6/1581185883299/DSCF5664.jpg?format=2500w")`, backgroundRepeat: 'no-repeat', width: "100%", height: 550, color: 'white'
          }}>
          <Navbar />

          <Row >
            <Col span={10} >
              <LeftOutlined
                onClick={() => history.goBack()}
                style={{ marginLeft: '4%', fontSize: '30px', color: 'black', }} />
            </Col>
            <Col span={12}>
              <ButtonLogin />
            </Col>
            <Col />
          </Row>
          <div style={{ marginLeft: '5%', marginRight: '50%', marginTop: '10%', borderWidth: 10, borderColor: 'white', border: '2px solid white' }}>
            <div style={{ padding: '2%' }}>
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', marginTop: '5%' }}>
                <Title style={{ color: 'white' }} >GALLERY</Title>
              </div>
              <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                <Title style={{ color: 'white', fontSize: 50 }} >C R E A T E</Title>
              </div>
            </div>
          </div>
        </div>
      </Col>
      <Content>
        <Col spen={12} style={{ backgroundColor: 'black', paddingTop: '4%', paddingBottom: '4%' }}>
          <div style={{ marginLeft: '20%', marginRight: '20%', }}>
            <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
              <Title level={2} style={{ color: 'white', }}>Update your lifestyle</Title>
            </div>
          </div>
        </Col>

        <Row style={{ margin: '5%', border: '5px solid white ', }}>
          <Col span={24} style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', padding: '2%', marginLeft: '5%' }}>
            <div className="clearfix">
              <Upload
                beforeUpload={() => { return false }}
                multiple
                listType="picture-card"
                className="avatar-uploader"
                onChange={({ fileList }) => {
                  setFileList(fileList)
                  setantimage(fileList.map((f) => f.originFileObj))
                  console.log('tttt', antimage)
                }}
              >
                {uploadButton}
              </Upload>
              <Button
                shape="round"
                disabled={antimage.length === 0}
                style={{ backgroundColor: 'transparent', color: 'black', borderColor: 'black', height: 40, width: 100 }}
                icon={<UploadOutlined />}
                onClick={onClickUpload} >
                Upload
              </Button>
            </div>
          </Col>
        </Row>
        {imageall.length !== 0 ?
          <Row style={{ margin: '5%', display: 'flex', alignItems: 'center', justifyContent: 'center', border: '5px solid white ', borderColor: 'white' }}>
            {imageall.map((item) => (
              <Card.Grid style={{ display: "flex", flexFlow: "column wrap", width: "400px" }}>
                <Badge onClick={() => onClickDeleteImg(item)}  >
                  <CloseOutlined style={{ padding: 5, backgroundColor: 'red', color: 'white', borderRadius: 20 }} />
                </Badge>
                <div style={{ display: 'flex', alignItems: 'center', justifyContent: 'center', }}>
                  <img alt="example" src={"http://localhost:8080" + item.path} style={{ width: "300px", height: '70%' }} />
                </div>
              </Card.Grid>
            )
            )}
          </Row>
          :
          <Empty />
        }

      </Content>
      <Footer style={{ textAlign: 'center' }}> Design ©2020 Created by Attapol Suwanno 602110181</Footer>
    </Layout >
  )

}
export default (Create)