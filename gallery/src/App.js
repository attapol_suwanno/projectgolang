import React from 'react';
import logo from './logo.svg';
import './App.css';
import Home from '../src/screens/Home/main'

function App() {
  return (
    <div className="App">
      <Home />
    </div >
  );
}

export default App;
