import axios from "axios";
import { Alert } from "antd";

const host = "http://localhost:8080";

// const host ="../api"

export function createAlbum(name) {
    const token = localStorage.getItem('token')
    return axios.post(`${host}/auth/galleries`, name, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function listAlbum() {
    const token = localStorage.getItem('token')
    return axios.get(`${host}/auth/galleries`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}
export function UpdateNameAlbum(item, id) {
    console.log('check id', id)
    console.log('check mname', item.name)
    console.log('check descip', item.desciption)
    const token = localStorage.getItem('token')
    return axios.patch(`${host}/auth/galleries/${id}`, { name: item.name, desciption: item.desciption }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function UpdateStatusAlbum(status, item) {
    console.log('check id', item.ID)
    console.log('check Status', status)
    const token = localStorage.getItem('token')
    return axios.patch(`${host}/auth/galleries/${item.ID}/status`, { status: status }, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function DeleteAlbum(album) {
    const token = localStorage.getItem('token')
    return axios.delete(`${host}/auth/galleries/${album.ID}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function UserSignup(singin) {
    console.log('hi', singin)
    return axios.post(`${host}/signup`, singin);
}

export function UserLogin(login) {
    console.log('hi', login)
    return axios.post(`${host}/login`, login);
}

export function UserLogOut() {
    const token = localStorage.getItem('token')
    console.log("ttttt: ", token);

    return axios.patch(`${host}/auth/logout`, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function ActiveView(id, count) {
    return axios.patch(`${host}/active/${id}`, { active: count });
}

export function listAlbumAll(id) {
    return axios.get(`${host}/gallerieall`);
}

export function ImageOfAlbum(id) {
    return axios.get(`${host}/gallerieall/${id}`);
}

export function UploadImage(album, IdAlum) {
    console.log('id', IdAlum)
    const token = localStorage.getItem('token')
    return axios.post(`${host}/auth/galleries/${IdAlum}/images`, album, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    });
}

export function listImage(IdAlum) {
    const token = localStorage.getItem('token')
    return axios.get(`${host}/auth/galleries/${IdAlum}/images`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function DeleteImage(id) {
    const token = localStorage.getItem('token')
    return axios.delete(`${host}/auth/images/${id}`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}

export function GetProfile() {
    const token = localStorage.getItem('token')
    return axios.get(`${host}/auth/profile`, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}